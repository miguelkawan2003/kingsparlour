<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkingTimes extends Model
{
    use HasFactory;

    protected $table = 'working_times';

    protected $fillable = ['employee_id', 'start_time', 'end_time'];
}

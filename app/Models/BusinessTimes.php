<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessTimes extends Model
{
    use HasFactory;

    protected $table = 'business_times';

    protected $fillable = ['start_time', 'end_time'];
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Employee::all();

        return response()->json($employee);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'phone' => 'required',
            'role' => 'required',
        ]);
        if ($validated->fails()) {
            return response()->json([
                'message' => 'Your request is missing data'
            ], 400);
       }

       $employee = Employee::create($request->all());

       return response()->json([
           'message' => 'Employee created successfully',
           'employee' => $employee
       ], 201);

    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->update($request->all());

        return response()->json([
            'message' => "Employee updated successfully!",
            'employee' => $employee
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return response()->json([
            'message' => "Employee deleted successfully!",
    ], 200);

    }

    public function getScheduleByEmployee(int $employee) {
        $employee = Employee::find($employee);
        $schedules = Schedule::where('employee_id', $employee->id)->get();
        
        return response()->json([
            "message" => "Sucesso na listagem",
            "data" => $schedules
        ], 200);
    }
}

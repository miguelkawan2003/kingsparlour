<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BusinessTimes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BusinessTimesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businessTimes = BusinessTimes::all();

        return response()->json($businessTimes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'start_time' => 'required',
            'end_time' => 'required',
        ]);
        if ($validated->fails()) {
            return response()->json([
                'message' => 'Your request is missing data'
            ], 400);
       }

       $businessTimes = BusinessTimes::create($request->all());

       return response()->json([
           'message' => 'Business Times created successfully',
           'business_times' => $businessTimes
       ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BusinessTimes  $businessTimes
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessTimes $businessTimes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BusinessTimes  $businessTimes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessTimes $businessTimes)
    {
        $businessTimes->update($request->all());

        return response()->json([
            'message' => "BusinessTimes updated successfully!",
            'business_times' => $businessTimes
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BusinessTimes  $businessTimes
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessTimes $businessTimes)
    {
        $businessTimes->delete();

        return response()->json([
            'message' => "BusinessTimes deleted successfully!",
    ], 200);
    }
}
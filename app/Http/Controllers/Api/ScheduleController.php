<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Schedule;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedule = Schedule::all();

        return response()->json($schedule);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'proceeding_id' => 'required',
            'employee_id' => 'required',
            'customer_id' => 'required'
        ]);

        if ($validated->fails()) {
            return response()->json([
                'message' => 'Your request is missing data'
            ], 400);
        }
   
        $schedule = Schedule::create($request->all());
   
        return response()->json([
            'message' => 'Schedule created successfully',
            'schedule' => $schedule
        ], 201);
    }

    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schedule  $agendamento
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        return $schedule;
        // if (Schedule::where('id', $id)->exists()) {
        //     $schedule = Schedule::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
        //     return response($schedule, 200);
        // } else {
        //     return response()->json([
        //       "message" => "Schedule not found"
        //     ], 404);
        // }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schedule  $agendamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    { 
        return response()->json([
            'message' => "Schedule updated successfully!",
            'schedule' => $schedule
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schedule  $agendamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        $schedule->delete();

        return response()->json([
            'message' => "schedule deleted successfully!",
    ], 200);
    }

    public function getSchedule($id) {
        if (Schedule::where('id', $id)->exists()) {
            $schedule = Schedule::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($schedule, 200);
        } else {
            return response()->json([
              "message" => "Schedule not found"
            ], 404);
        }    
    }

    public function getScheduleByCustomer(int $customer) {
        $schedules = Schedule::where('customer_id', $customer);
        return response()->json([
            "message" => $schedule
        ], 404);
    } 


    
}

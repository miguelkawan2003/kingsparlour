<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Proceeding;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ProceedingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proceeding = Proceeding::all();

        return response()->json($proceeding);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'duration' => 'required',
            'value' => 'required'
        ]);
        if ($validated->fails()) {
            return response()->json([
                'message' => 'Your request is missing data'
            ], 400);
       }

       $proceeding = Proceeding::create($request->all());

       return response()->json([
           'message' => 'Proceeding created successfully',
           'proceeding' => $proceeding
       ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proceeding  $proceeding
     * @return \Illuminate\Http\Response
     */
    public function show(Proceeding $proceeding)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proceeding  $proceeding
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proceeding $proceeding)
    {
        $proceeding->update($request->all());

        return response()->json([
            'message' => "Proceeding updated successfully!",
            'proceeding' => $proceeding
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Proceeding  $proceeding
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proceeding $proceeding)
    {
        $proceeding->delete();

        return response()->json([
            'message' => "Proceeding deleted successfully!",
    ], 200);

    }
}

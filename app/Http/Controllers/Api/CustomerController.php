<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::all();

        return response()->json($customer);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required',
            'cpf' => 'required',
            'phone' => 'required',
            'user' => 'required',
            'password' => 'required' 
        ]);
        if ($validated->fails()) {
             return response()->json([
                 'message' => 'Your request is missing data'
             ], 400);
        }

        $customer = Customer::create($request->all());

        return response()->json([
            'message' => 'Customer created successfully',
            'customer' => $customer
        ], 201);

    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request 
     * @param  \App\Models\Customer 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $customer->update($request->all());

        return response()->json([
            'message' => "Customer updated successfully!",
            'customer' => $customer
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();

        return response()->json([
            'message' => "Customer deleted successfully!",
    ], 200);
    }

    public function getScheduleByCustomer(int $customer) {
        $customer = Customer::find($customer);
        $customers = Schedule::where('customer_id', $customer->id)->get();
        
        return response()->json([
            "message" => "Sucesso na listagem",
            "data" => $customers
        ], 200);
    }
}
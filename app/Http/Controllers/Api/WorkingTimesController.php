<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\WorkingTimes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WorkingTimesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workingTimes = WorkingTimes::all();

        return response()->json($workingTimes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'employee_id' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);
        if ($validated->fails()) {
            return response()->json([
                'message' => 'Your request is missing data'
            ], 400);
        }
        
        $workingTimes = WorkingTimes::create($request->all());

        return response()->json([
            'message' => 'Working Times created successfully',
            'working_times' => $workingTimes
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WorkingTimes  $workingTimes
     * @return \Illuminate\Http\Response
     */
    public function show(WorkingTimes $workingTimes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WorkingTimes  $workingTimes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkingTimes $workingTimes)
    {
        $workingTimes->update($request->all());

        return response()->json([
            'message' => "Working Times updated successfully!",
            'working_times' => $workingTimes
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WorkingTimes  $workingTimes
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkingTimes $workingTimes)
    {
        $workingTimes->delete();

        return response()->json([
            'message' => "Working Times deleted successfully!",
    ], 200);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->id();
            $table->date("date");
            $table->time("start_time");
            $table->time("end_time");
            $table->bigInteger('employee_id')->unsigned();
            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('proceeding_id')->unsigned();
            
            // $table->unsignedBigInteger("employee_id");
            // $table->unsignedBigInteger("costumers_id");
            
            
            $table->timestamps();

            // $table->foreign('employee_id')->references('id')->on('employees');
            // $table->foreign('costumers_id')->references('id')->on('costumers');
        });
        Schema::table('schedules', function($table){
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('proceeding_id')->references('id')->on('proceedings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
};

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ScheduleController;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\EmployeeController;
use App\Http\Controllers\Api\ProceedingController;
use App\Http\Controllers\Api\BusinessTimesController;
use App\Http\Controllers\Api\WorkingTimesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('schedule', ScheduleController::class);
Route::apiResource('customer', CustomerController::class);
Route::apiResource('employee', EmployeeController::class);
Route::apiResource('proceeding', ProceedingController::class);
Route::apiResource('business_times', BusinessTimesController::class);
Route::apiResource('working_times', WorkingTimesController::class);

Route::get('customer/{customer}/schedules', [CustomerController::class, 'getScheduleByCustomer']);
Route::get('employee/{employee}/schedules', [EmployeeController::class, 'getScheduleByEmployee']);

// Route::get('schedules/{id}', 'ScheduleController@getSchedule');

// Route::get('clientes', 'App\\Http\\Controllers\\Api\\ClienteController@getAll');
// Route::post('clientes', 'App\\Http\\Controllers\\Api\\ClienteController@createCliente');


